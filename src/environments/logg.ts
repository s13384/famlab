import { environment } from "./environment.prod";

export function logg(any): any {
    if(environment)
        console.log(any);
    return any;
}