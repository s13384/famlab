import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ShoppingListComponent } from './components/shopping-list/shopping-list.component';
import { ShopsComponent } from './components/shops/shops.component';
import { ShopDetailComponent } from './components/shop-detail/shop-detail.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LoginComponent } from './components/login/login.component';
import { ShoppingListsComponent } from './components/shopping-lists/shopping-lists.component';
import { ProductListComponent } from './components/product-list/product-list.component'

import { LoginGuard } from './guards/login-guard/login.guard';
import { RegisterComponent } from './components/register/register.component';
import { AddCommentComponent } from './components/add-comment/add-comment.component';
import { CategoryComponent } from './components/category/category.component';

const routes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'login', component: LoginComponent},
    { path: 'products', component: ProductsComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'productdetail/:id', component: ProductDetailComponent },
    { path: 'cart', component: ShoppingListComponent },
    { path: 'shops', component: ShopsComponent },
    { path: 'shopdetail/:id', component: ShopDetailComponent },
    { path: 'shoppinglists', component: ShoppingListsComponent, canActivate: [LoginGuard]},
    { path: 'productsearch/:name', component: ProductListComponent},
    { path: 'register', component: RegisterComponent},
    { path: 'comment', component: AddCommentComponent},
    { path: 'category/:id', component: CategoryComponent},
    { path: '**', component: PageNotFoundComponent }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}