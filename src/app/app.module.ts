import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductEditComponent } from './components/product-edit/product-edit.component';
import { ProductService } from './services/product-service/product.service';
import { ProductsComponent } from './components/products/products.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProductSearchComponent } from './components/product-search/product-search.component';
import { ShopsComponent } from './components/shops/shops.component';
import { CartInfoComponent } from './components/cart-info/cart-info.component';
import { ShoppingListComponent } from './components/shopping-list/shopping-list.component';
import { ShoppingListService } from './services/shopping-list-service/shopping-list.service';
import { ShopService } from './services/shop-service/shop.service';
import { ShopDetailComponent } from './components/shop-detail/shop-detail.component';
import { ProductSearchService }  from './services/product-search-service/product-search.service';
import { LoginService } from './services/login-service/login.service';

import { LoginGuard } from './guards/login-guard/login.guard';

import { AppRoutingModule } from './app-routing.module';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './services/in-memory-data-service/in-memory.data.service';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LoginComponent } from './components/login/login.component';
import { ShoppingListsComponent } from './components/shopping-lists/shopping-lists.component';
import { StringsService } from './services/strings-service/strings.service';
import { ProductListComponent } from './components/product-list/product-list.component';
import { RegisterComponent } from './components/register/register.component';
import { MapComponent } from './components/map/map.component';
import { AddCommentComponent } from './components/add-comment/add-comment.component';
import { CommentService } from './services/comment/comment.service';
import { CategoryComponent } from './components/category/category.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { PaginationService } from './services/pagination-service/pagination.service'

@NgModule({
  imports: [ 
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, {passThruUnknownUrl: true})
  ],
  declarations: [ 
    AppComponent,
    ProductDetailComponent,
    ProductEditComponent,
    ProductsComponent,
    DashboardComponent,
    ProductSearchComponent,
    ShoppingListComponent,
    ShopsComponent,
    ShopDetailComponent,
    CartInfoComponent,
    PageNotFoundComponent,
    LoginComponent,
    ShoppingListsComponent,
    ProductListComponent,
    RegisterComponent,
    MapComponent,
    AddCommentComponent,
    CategoryComponent,
    CategoriesComponent,
  ],
  providers: [ 
    ProductService,
    ShoppingListService,
    ShopService,
    LoginService,
    StringsService,
    LoginGuard,
    ProductSearchService,
    CommentService,
    PaginationService
  ],
  bootstrap: [ 
    AppComponent 
  ]
})
export class AppModule { }