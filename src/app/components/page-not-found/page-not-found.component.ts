import { Component, OnInit } from '@angular/core';
import { StringsService } from '../../services/strings-service/strings.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  constructor(private strings: StringsService) { }

  ngOnInit() {
  }

}
