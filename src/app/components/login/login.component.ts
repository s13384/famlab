import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
//import { FormGroup, FormControl } from '@angular/forms';
import { LoginService } from '../../services/login-service/login.service';
import { StringsService } from '../../services/strings-service/strings.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  //form;
  ifInfo: boolean;
  info: string;
  @ViewChild('login') private loginInput: ElementRef;

  constructor(private loginService: LoginService, private router: Router, private strings: StringsService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.loginInput.nativeElement.focus();
    console.log("uname:" + localStorage.getItem('username') + " token: " + localStorage.getItem('token'))
  }

  onSubmit(user): void {
    if (user.name.length > 4 && user.password.length > 4) {
      this.tryLogin(user);
    }
  }

  tryLogin(user): void {
    this.loginService.login(user.name, user.password).then(isSuccesful => {
      if (isSuccesful) {
        this.ifInfo = false;
        let link = ['/products'];
        this.router.navigate(link);
      }
      else {
        this.ifInfo = true;
        this.info = 'wrong username or password';
      }
    }).catch(e => {
      this.ifInfo = true;
      this.info = 'wrong username or password';
      console.log(e)
    });
  }

  register(): void {
    this.router.navigate(['/register'])
  }

}
