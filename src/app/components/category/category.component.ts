import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product-service/product.service';
import { Category } from '../../classes/category';
import { Router, ActivatedRoute } from '@angular/router';
import { ParamMap } from '@angular/router/src/shared';
import { Product } from '../../classes/product';
import { ProductInfo } from '../../classes/product-info';

@Component({
  selector: 'category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(private productService: ProductService,
          private router: Router,
          private route: ActivatedRoute) { }

  products: ProductInfo[]
  categoryName: string

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) => {return this.productService.getProductsWithCategory(+params.get('id')).then(prods => this.products = prods)})
      .subscribe()
    this.route.queryParams.subscribe((params) => this.categoryName=params['categoryName'])
  }

  gotoDetail(product: ProductInfo): void {
    this.router.navigate(['/productdetail', product.id]);
}

}
