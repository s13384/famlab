import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product-service/product.service';
import { Category } from '../../classes/category';
import { Router } from '@angular/router';

@Component({
  selector: 'categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  constructor(private productService: ProductService,
          private router: Router) { }

  categories: Category[]

  ngOnInit() {
    this.productService.getCategories().then(cats => this.categories = cats)
  }

  gotoCategory(category : Category): void {
    this.router.navigate(['/category', category.id], { queryParams: {categoryName: category.name}})
  }

}
