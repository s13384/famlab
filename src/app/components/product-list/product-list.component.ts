import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { ProductService } from '../../services/product-service/product.service';
import { Product } from '../../classes/product';
import { ProductSearchService } from '../../services/product-search-service/product-search.service';
import { ProductInfo } from '../../classes/product-info';
import { PaginationService } from '../../services/pagination-service/pagination.service';
import { logg } from '../../../environments/logg';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  name: string
  products: ProductInfo[]
  selectedProduct: ProductInfo
  searched: string

  constructor(private route: ActivatedRoute,
            private router: Router,
            private productSearch: ProductSearchService,
            private paginationService: PaginationService) { }

  ngOnInit() {
    this.route.paramMap
    .switchMap((params: ParamMap) => {
      this.searched = params.get('name'); 
      return this.productSearch.searchByName(params.get('name'), this.currentPage)
    })
    .catch(e => {console.log(e); return null;})
    .subscribe((prods: {products: ProductInfo[], maxPages: number}) => {
      this.products = logg(prods.products)
      this.maxPages = prods.maxPages
      this.pageNumbers()
    })

    this.route.queryParamMap.subscribe((paramMap : ParamMap) => { 
      this.currentPage = paramMap.has('page') != null ? +paramMap.get('page') : 1
      console.log("current Page:" + this.currentPage + " hasPage?" + paramMap.get('page'))
      this.changeCurrentPage(this.currentPage)
    })
  }


  // pagination stuff
  currentPage: number = 1
  itemsPerPage: number = 10
  maxPages: number = 1
  pages: number[]

  changeCurrentPage(page: number): void {
    if (page < 1 || page > this.maxPages)
      return;
    //this.currentPage = page;
    this.pageNumbers()
    //this.getProducts(page)
  }

  pageNumbers(): void {
    console.log("Currpg:"+ this.currentPage + " Maxpages:"+ this.maxPages)
    this.pages = this.paginationService.pageNumbers(this.currentPage, this.maxPages) 
    console.log(this.pages)
  }
}
