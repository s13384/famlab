import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

import { Shop } from '../../classes/shop';
import { Product } from '../../classes/product';
import { ProductService } from '../../services/product-service/product.service';
import { ShopService} from '../../services/shop-service/shop.service';
import { ShoppingListService } from '../../services/shopping-list-service/shopping-list.service';
import { StringsService } from '../../services/strings-service/strings.service';
import { Router } from '@angular/router';
import { logg } from '../../../environments/logg';
import { ProductInfo } from '../../classes/product-info';

import swal from 'sweetalert';

@Component({
    selector: 'product-detail',
    templateUrl: './product-detail.component.html',
    styleUrls: [ './product-detail.component.css' ]
})

export class ProductDetailComponent implements OnInit { 

    @Input() productInfo: ProductInfo;
    @Input() showAddToCartButton: boolean;
    @Input() showCartInfo: boolean;
    @Input() product: Product;
    @Input() products: Product[] | any;
    //shops: Shop[];
    constructor(
      private productService: ProductService,
      private shoppingList: ShoppingListService,
      private shopService: ShopService,
      private route: ActivatedRoute,
      private location: Location,
      private strings: StringsService,
      private router: Router
    ){
        this.showAddToCartButton=true;
        this.showCartInfo=true;
    }

    ngOnInit(): void {
        if (this.productInfo == undefined || this.productInfo == null)
        {
        console.log("If")
        this.route.paramMap
          .switchMap((params: ParamMap) => this.productService.getProduct(+params.get('id'))//.then(product => this.product = logg(product).productInfo) 
          .catch(e => { console.log(e); /*this.router.navigate(['/products']) */ }))
          .subscribe(product => {
            this.productInfo = logg(product)[0].productInfo;
            this.product = logg(product[0])
            this.products = product
          });
        }
        else {
          console.log("Else")
          this.productService.getProduct(this.productInfo.id).then(p => {
            console.log(p)
            this.product = logg(p[0]); 
            this.products = logg(p)
        })
      }

      //this.shopService.getShops().then(shops => this.shops = shops);
    }

    addToCart(product: Product): void{
        this.shoppingList.addProduct(product);
        
        swal({
          title: "Dodano do listy",
          text: product.productInfo.name,
          icon: "success",
          button: "Super!",
        });
      }
    }