
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';

import { Product } from '../../classes/product';
import { Shop } from '../../classes/shop';
import { ShopService } from '../../services/shop-service/shop.service';

import { ProductService } from '../../services/product-service/product.service';
import { StringsService } from '../../services/strings-service/strings.service';
import { ProductInfo } from '../../classes/product-info';


@Component({
    selector: 'shop-detail',
    templateUrl: './shop-detail.component.html',
    styleUrls: ['./shop-detail.component.css']
})
export class ShopDetailComponent {

    @Input() shop: Shop | any;
    products: ProductInfo[];
    constructor(
      private shopService: ShopService,
      private route: ActivatedRoute,
      private location: Location,
      private productService: ProductService,
      private strings: StringsService,
      private router: Router
    ){}

    ngOnInit(): void{
      this.route.paramMap
      .switchMap((params: ParamMap) => this.shopService.getShop(+params.get('id'))
      .catch(e => {console.log(e); this.router.navigate(['/shops'])}))
      .subscribe(shop => {console.log(shop); this.shop = shop; this.getProductsForShop(this.shop.id)});

    }

    getProductsForShop(id: number){
        this.productService.getProductsForShop(id).then(prods => this.products = prods)
    }

    goBack(): void {
      this.location.back();
    }

    save(): void {
      this.shopService.update(this.shop)
        .then(() => this.goBack());
    }
    
    gotoDetail(product: Product): void {
        let link = ['/productdetail', product.id];
        this.router.navigate(link);
    }
}