import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';

import { Product } from '../../classes/product';
import { ProductService } from '../../services/product-service/product.service';

@Component({
    selector: 'product-edit',
    templateUrl: './product-edit.component.html',
    styleUrls: [ './product-edit.component.css' ]
})

export class ProductEditComponent implements OnInit { 

    @Input() product: Product;
    constructor(
      private productService: ProductService,
      private route: ActivatedRoute,
      private location: Location
    ){}

    ngOnInit(): void{
      /*this.route.paramMap
      .switchMap((params: ParamMap) => this.productService.getProduct(+params.get('id')))
      .subscribe(product => this.product = product);*/
    }

    goBack(): void {
      this.location.back();
    }

    save(): void {
      /*this.productService.update(this.product)
        .then(() => this.goBack());*/
    }
}