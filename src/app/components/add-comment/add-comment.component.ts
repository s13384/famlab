import { Component, OnInit, Input } from '@angular/core';
import { CommentService } from '../../services/comment/comment.service';
import { StringsService } from '../../services/strings-service/strings.service';

@Component({
  selector: 'add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnInit {

  constructor(private commentService: CommentService,
            private strings: StringsService ) { }

  @Input() productId: number
  @Input() userId: number

  ngOnInit() {
  }

  onSubmit(comment): void {
    console.log(comment.comment)
    this.commentService.addComment(this.productId , this.userId,comment.comment)
  }

}
