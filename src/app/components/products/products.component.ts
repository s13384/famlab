import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from '../../classes/product'; 
import { ProductService } from '../../services/product-service/product.service';
import { ShoppingListService } from '../../services/shopping-list-service/shopping-list.service';

import { Observable } from 'rxjs/Observable';
import { ProductInfo } from '../../classes/product-info';
import { ParamMap } from '@angular/router/src/shared';
import { PaginationService } from '../../services/pagination-service/pagination.service';

@Component({
  selector: 'my-product',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
providers: [ ],
})
export class ProductsComponent implements OnInit  { 
  name = 'Angular';
  selectedProduct: ProductInfo;
  products: Product[];
  numberOfProducts: Observable<number>;
  number: number;

  detailProduct: Product
  detailProducts: Product[]

  constructor(private productService :ProductService, private router: Router, 
      private shoppingList: ShoppingListService,
      private route: ActivatedRoute,
      private paginationService: PaginationService )
  { }

  
  onSelect(product: ProductInfo): void {
    this.productService.getProduct(product.id).then(p => {
      this.detailProduct = p[0]; 
      this.detailProducts = p  
      this.selectedProduct = product;
    })
  }

  getProducts(): void {
    this.number=this.shoppingList.getNumberOfProducts();
    this.productService.getProducts(this.currentPage).then(products => {
      this.products = products.products
      this.maxPages=products.maxPages
      this.pageNumbers()
    })
  }

  ngOnInit() {
    this.getProducts();
    this.route.queryParamMap.subscribe((paramMap : ParamMap) => { 
      this.currentPage = paramMap.has('page') != null ? +paramMap.get('page') : 1
      this.changeCurrentPage(this.currentPage)
    })
  }

  gotoDetail(): void {
    this.router.navigate(['/productdetail', this.selectedProduct.id]);
  }

  addToCart(product: Product): void{
    this.shoppingList.addProduct(product);
    this.number=this.shoppingList.getNumberOfProducts();
  }

  gotoCart(): void {
    let link = ['/cart'];
    this.router.navigate(link);
  }

  // pagination stuff
  currentPage: number = 1
  itemsPerPage: number = 10
  maxPages: number = 1111
  pages: number[]

  changeCurrentPage(page: number): void {
    if (page < 1 || page > this.maxPages)
      return;
    this.currentPage = page;
    this.pageNumbers()
    this.getProducts()
  }

  pageNumbers(): void {
    this.pages = this.paginationService.pageNumbers(this.currentPage, this.maxPages) 
  }
}