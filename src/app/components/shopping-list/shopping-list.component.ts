import { Component, Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../../classes/product';
import { ShoppingListService } from '../../services/shopping-list-service/shopping-list.service';
import { ShoppingListItem } from '../../classes/shopping-list-item';
import { Shop } from '../../classes/shop';
import { ShopService } from '../../services/shop-service/shop.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { StringsService } from '../../services/strings-service/strings.service';
import { logg } from '../../../environments/logg';


@Component({
    selector: 'shopping-list',
    templateUrl: './shopping-list.component.html',
    styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {  

    selectedProduct: Product;
    shops: Shop[];

    constructor(private shoppingList:ShoppingListService,
        private router: Router,
        private shopsService: ShopService,
        private strings: StringsService){}

    products: Observable<ShoppingListItem[]>;

    ngOnInit(): void {
        this.products = this.shoppingList.getProducts().map(products => logg(products) as ShoppingListItem[]);
        this.shopsService.getShops().then(shops => this.shops = shops);
        this.shoppingList.changed.subscribe(b => this.refresh())
    }

    private refresh(): void {
        this.shoppingList.getCurrentShoppingList().then(s => {console.log("2222"); this.products = Observable.of(logg(s.products))});
      }

    gotoDetail(product: Product): void {
        this.router.navigate(['/productdetail', product.productInfo.id]);
        this.selectedProduct = product;
    }

    toggleBought(shoppingListItem: ShoppingListItem): void {
        console.log(shoppingListItem.product.id)
        this.shoppingList.toggleBought(shoppingListItem).then(shp => this.products = Observable.of(shp.products))
    }

    removeOne(product: Product): void {
        //this.shoppingList.removeProduct(product);
        //this.products = this.shoppingList.getProducts().map(products => products as ShoppingListItem[]);
        //this.refresh();
        this.shoppingList.removeProduct(product).then(shpli =>{console.log("shp li comp"); this.products = Observable.of(logg(shpli.products))});
        this.products = this.shoppingList.getProducts().map(products => products as ShoppingListItem[]);
    }

    addOne(product: Product): void{
        this.shoppingList.addProduct(product).then(shpli =>{console.log("shp li comp"); this.products = Observable.of(logg(shpli.products))});
        this.products = this.shoppingList.getProducts().map(products => products as ShoppingListItem[]);
        //this.refresh()
    }

    /*removeProducts(product: ShoppingListItem): void {
        this.shoppingList.removeProducts(product)
    }*/
}