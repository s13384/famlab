import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { Product } from '../../classes/product';
import { ShoppingList } from '../../classes/shopping-list';
import { ShoppingListItem } from '../../classes/shopping-list-item';
import { User } from '../../classes/user';

import { ShoppingListService } from '../../services/shopping-list-service/shopping-list.service';
import { StringsService } from '../../services/strings-service/strings.service';
import { logg } from '../../../environments/logg';
import { Subject } from 'rxjs/Subject';

import swal from 'sweetalert';

@Component({
  selector: 'shopping-lists',
  templateUrl: './shopping-lists.component.html',
  styleUrls: ['./shopping-lists.component.css']
})
export class ShoppingListsComponent implements OnInit {

  shoppingLists: ShoppingList[];
  currentShoppingList: ShoppingList;
  @ViewChild('listname') private listNameInput: ElementRef;

  constructor(private shoppingListService: ShoppingListService,
              private strings: StringsService) { }

  ngOnInit() {
    this.shoppingListService.changed.subscribe(b => {this.refresh()})
    //this.shoppingListService.getShoppingLists().then(s => {console.log("1111"); this.shoppingLists = logg(s)});
    //this.shoppingListService.getCurrentShoppingList().then(s => {console.log("2222"); this.currentShoppingList = logg(s)});
    this.shoppingListService.gettCurrentShoppingList().subscribe(s => this.currentShoppingList = s)
    this.shoppingListService.gettShoppingLists().subscribe(s => this.shoppingLists = s);
    console.log("currshp list")
    console.log(this.currentShoppingList)
  }

  private refresh(): void {
    this.shoppingListService.getShoppingLists().then(s => {console.log("1111"); this.shoppingLists = logg(s)});
    this.shoppingListService.getCurrentShoppingList().then(s => {console.log("2222"); this.currentShoppingList = logg(s)});
  }

  onUserChane(u: User): void {
    this.shoppingListService.getShoppingLists().then(s => this.shoppingLists = s);
    this.shoppingListService.getCurrentShoppingList().then(s => this.currentShoppingList = s);
  }

  changeCurrentShoppingList(id: number): void {
    console.log("changing list")
    this.currentShoppingList = this.shoppingListService.changeShoppingList(id);
    console.log("current list")
    console.log(this.currentShoppingList)
    //this.shoppingListService.getCurrentShoppingList().then(s => this.currentShoppingList = s);
  }

  removeOne(product: Product): void {
    this.shoppingListService.removeProduct(product);
  }

  addOne(product: Product): void{
    this.shoppingListService.addProduct(product);
  }

  createShoppingList(): void {
    this.shoppingListService.createShoppingList();
    this.shoppingListService.getCurrentShoppingList().then(s => this.currentShoppingList = s);
  }

  changeListName(newName: string): void {
    if (newName.length > 0) {
      this.shoppingListService.renameCurrentShoppingList(newName).then(s => this.shoppingLists = s)
    } 
    else {
      newName = this.strings.UnnamedList
      this.shoppingListService.renameCurrentShoppingList(newName).then(s => this.shoppingLists = s)
    }
    this.listNameInput.nativeElement.value = '';
  }

  deleteShoppingList(shpLiId: number): void {
    console.log("DELETING:"+ shpLiId)
    // if(window.confirm(this.strings.ListRemoval))
    //   this.shoppingListService.deleteShoppingList(shpLiId)

      swal({
        title: "Na pewno?",
        text: this.strings.ListRemoval,
        icon: "warning",
        buttons: ["Nie!", "Tak"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          swal("Lista usunięta!", {
            icon: "success",
            button: "Super!",
          });
          this.shoppingListService.deleteShoppingList( shpLiId );
        } 
      });
  }

  private reciveShoppingLists(shpLists: ShoppingList[]): void {
    console.log("shopping list component - lists:")
    console.log(this.shoppingLists)
    this.shoppingLists = logg(shpLists)
  }

}
