import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../../classes/product';
import { ProductService } from '../../services/product-service/product.service'
import { Shop } from '../../classes/shop';
import { ShopService } from '../../services/shop-service/shop.service';

import { Observable } from 'rxjs/Observable';
import { logg } from '../../../environments/logg';


@Component({
    selector: 'shops',
    templateUrl: './shops.component.html',
    styleUrls: ['./shops.component.css']
})
export class ShopsComponent implements OnInit {

    shops: Shop[];

    constructor(private shopService:ShopService, private productService:ProductService,
            private router:Router){}

    ngOnInit(): void {
        this.getShops();
    }

    getShops(): void {
        this.shopService.getShops().then(shops => {console.log("shops"); this.shops = logg(shops);});
    }

    shopDetails(shop: Shop): void {
        this.router.navigate(['/shopdetail', shop.id]);
    }

    productDetail(product: Product): void {
        this.router.navigate(['/productdetail', product.id]);
    }

    shopDetail(shop: Shop): void {
        this.router.navigate(['/shopdetail', shop.id])
    }
}