import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/observable/of';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { ProductSearchService } from '../../services/product-search-service/product-search.service';
import { Product } from '../../classes/product';
import { StringsService } from '../../services/strings-service/strings.service';


@Component({
    selector: 'product-search',
    templateUrl: './product-search.component.html',
    styleUrls: ['./product-search.component.css'],
    providers: [ProductSearchService]
})
export class ProductSearchComponent implements OnInit {

    products: Observable<Product[]>;
    private searchTerms = new Subject<string>();
    private searchPrices = new Subject<number>();
    searchType: string;

    constructor(private productSearchService: ProductSearchService,
                private router: Router,
                private strings: StringsService){ }

    search(term: string): void{
        //if (this.searchType==="Name")
            this.searchTerms.next(term);
        //else 
        //    this.searchPrices.next(+term);
    }

    ngOnInit(): void{
        /*this.products = this.searchTerms
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(term => term ? this.productSearchService.searchByName(term) : Observable.of<Product[]>([]))
            .catch(error => {
                console.log(error);
                return Observable.of<Product[]>([]);
            });*/
        this.searchType="Name";
    }

    gotoDetail(product: Product): void {
        let link = ['/productdetail', product.id];
        this.router.navigate(link);
    }

    changeSearchType(type: string){
        this.searchType = type;
    }

    findProducts(prodName: string): void {
        this.router.navigate(['productsearch', prodName], {queryParams: {page: '1'}})
    }
}