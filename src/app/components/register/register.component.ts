import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { StringsService } from '../../services/strings-service/strings.service';
import { LoginService } from '../../services/login-service/login.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, AfterViewInit {

  @ViewChild('focus') private username: ElementRef

  constructor(private strings: StringsService,
              private loginService: LoginService,
              private router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.username.nativeElement.focus()
  }

  onSubmit(user): void {
    this.loginService.registerUser(user.username, user.email, user.password).  
      then(result => this.router.navigate(['/login'])).catch(e => console.log(e))
  }

}
