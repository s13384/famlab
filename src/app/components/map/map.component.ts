import { Component, OnInit , Input} from '@angular/core';
import { navigationCancelingError } from '@angular/router/src/shared';

declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  constructor() {
  }

  @Input() shopLat: number;
  @Input() shopLong: number;

  static shpLat
  static shpLong
  static directionsService :any ;
  static directionsDisplay : any ;
  static map: any;
  static homelat = undefined;
  static homelng = undefined;

  ngOnInit() {
    MapComponent.shpLat = this.shopLat
    MapComponent.shpLong = this.shopLong
    console.log("shpLat:" + this.shopLat + " shpLong:" + this.shopLong)
    console.log(this)
    
    MapComponent.directionsService = new google.maps.DirectionsService;
    MapComponent.directionsDisplay = new google.maps.DirectionsRenderer;
    MapComponent.map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: {lat: 54.3553792, lng: 18.644575}
    });
    MapComponent.directionsDisplay.setMap(MapComponent.map);

      
    //c = getPos;
    if (!navigator.geolocation){
      console.log("TRUE")
      MapComponent.doMap(undefined, undefined)
    }else {
      console.log("FALSE")
      navigator.geolocation.getCurrentPosition(this.c);
    }
  }

  c(pos): void{
    console.log(pos)
    console.log(this)
    MapComponent.homelat = pos.coords.latitude;
    MapComponent.homelng = pos.coords.longitude;
    console.log('homelat:' + MapComponent.homelat)
    console.log('homelong:' + MapComponent.homelng)
    //map.setPosition({lat: homelat, lng: homelng})
    MapComponent.doMap(MapComponent.homelat, MapComponent.homelng)
    //calculateAndDisplayRoute(directionsService, directionsDisplay, this.shopLat, this.shopLong, homelat, homelng);
  };  

  static doMap(homelat, homelng):void {
    this.calculateAndDisplayRoute(MapComponent.directionsService, MapComponent.directionsDisplay, 
      MapComponent.shpLat, MapComponent.shpLong, homelat, homelng);
  }

      
  static calculateAndDisplayRoute(directionsService, directionsDisplay, shpLat, shpLong, homelat, homelng) {

    let waypts = [];
    let checkboxArray: any[] = [
    ];
    for (let i = 0; i < checkboxArray.length; i++) {

      waypts.push({
        location: checkboxArray[i],
        stopover: true
      });

    }

    homelat = homelat == undefined ? 54.3553792 : homelat
    homelng = homelng == undefined ? 18.644575 : homelng

    directionsService.route({
      origin: {lat: homelat, lng: homelng},
      destination: {lat: shpLat /*54.3564072*/ /*this.shopLat*/, lng: shpLong /*18.6487937*/ /*this.shopLong*/},
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function (response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }
}