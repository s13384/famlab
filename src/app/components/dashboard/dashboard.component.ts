import { Component, OnInit } from '@angular/core';

import { Product } from '../../classes/product';
import { ProductService } from '../../services/product-service/product.service';
import { StringsService } from '../../services/strings-service/strings.service';

import { Router } from '@angular/router';
@Component({
    selector: 'my-dashborad',
    templateUrl: './dashboard.component.html',
    styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit{

    products: Product[];
    constructor(private productService: ProductService,
        private strings: StringsService,
        private router: Router ){}
    ngOnInit(): void{
        this.productService.getProducts(1).then(products => {
            this.products = products.products.slice(0,4)
        });
    }

    gotoDetail(product: Product): void {
        let link = ['/productdetail', product.id];
        this.router.navigate(link);
    }
}