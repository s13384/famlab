import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { ShoppingListService } from '../../services/shopping-list-service/shopping-list.service';
import { ShoppingList } from '../../classes/shopping-list';
import { StringsService } from '../../services/strings-service/strings.service';
import { logg } from '../../../environments/logg';
import swal from 'sweetalert';


@Component({
    selector: 'cart-info',
    templateUrl: './cart-info.component.html',
    styleUrls: ['./cart-info.component.css']
})
export class CartInfoComponent {

    constructor(private shoppingService: ShoppingListService,
                private router: Router,
                private strings: StringsService) { }

    ngOnInit():void {
    }

    gotoCart(): void {
        let link = ['/cart'];
        this.router.navigate(link);
    }

    
    clearWholeList(): void {
        // if (window.confirm(this.strings.ListRemoval))
        //     this.shoppingService.clearShoppingLists()

        swal({
            title: "Na pewno?",
            text: this.strings.ListRemoval,
            icon: "warning",
            buttons: ["Nie!", "Tak"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                swal("Lista wyczyszczona!", {
                    icon: "success",
                    button: "Super!",
                });
            this.shoppingService.clearShoppingLists();
            }
        });
    }

}