import { Injectable } from '@angular/core';
import { Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Product } from '../../classes/product';
import { environment } from '../../../environments/environment.prod';
import { logg } from '../../../environments/logg';
import { apiUrl } from '../../../environments/apiUrl';
import { ProductInfo } from '../../classes/product-info';
import { Category } from '../../classes/category';

@Injectable()
export class ProductService {

    constructor(private http: Http){}

    getProducts(page: number): Promise<{products: Product[], maxPages: number}> {
        let url = apiUrl + `/products/${page}`;
        return this.http.get(url)
            .toPromise()
            .then(response => {return {products: logg(response.json()).content as Product[], maxPages: response.json().totalPages as number}})
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    getProduct(id: number): Promise<Product[]> {
        const url = apiUrl + `/products/base?baseId=${id}`;
        return this.http.get(url)
          .toPromise()
          .then(response => logg(response).json().products as Product[])
          .catch(this.handleError);
    }

    getProductsForShop(shopId: number): Promise<ProductInfo[]> {
        let url = apiUrl + '/shops/' + shopId + '/products';
        return this.http.get(url)
            .toPromise()
            .then(response => logg(response).json().products as ProductInfo[])
            .catch(this.handleError);
    }

    getCategories(): Promise<Category[]> {
        return this.http.get(apiUrl + '/categories').toPromise()
            .then(categories => categories.json().categories as Category[])
            .catch(this.handleError)
    }

    getProductsWithCategory(categoryId: number): Promise<ProductInfo[]> {
        return this.http.get(apiUrl + `/products?categoryId=${categoryId}`).toPromise()
            .then(prods => prods.json().products as ProductInfo[])
            .catch(this.handleError)
    }

    /*
    private headers = new Headers({'Content-Type' : 'application/json'});
    update(product: Product): Promise<Product> {
        const url = `${this.productsUrl}/${product.id}`;
        return this.http
        .put(url, JSON.stringify(product), {headers: this.headers})
        .toPromise()
        .then(()=> product)
        .catch(this.handleError);
    }

    create(name: string, description: string, price: number): Promise<Product> {
        return this.http
            .post(this.productsUrl, JSON.stringify({name: name, description: description, price: price}), {headers: this.headers})
            .toPromise()
            .then(res => res.json().data as Product)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        const url = `${this.productsUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
        .toPromise()
        .then(()=>null)
        .catch(this.handleError);
    }
    */
}