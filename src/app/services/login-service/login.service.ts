import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http'

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { Login } from '../../classes/login';
import 'rxjs/add/operator/toPromise';
import { logg } from '../../../environments/logg';
import { apiUrl } from '../../../environments/apiUrl'
import sha1 from 'sha1'

@Injectable()
export class LoginService {

  private userName: string;
  private isLoggedIn: boolean;
  public subject: Subject<string> = new Subject; 

  constructor(
  private http: Http) {
    this.isLoggedIn = false;
    this.userName = "";
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private userAuth: string = apiUrl + '/auth'
  private headers = new Headers({'Content-Type' : 'application/json'});

  login(userName: string, password: string): Promise<boolean> {
    return this.http.post(this.userAuth, JSON.stringify(new Login(userName, sha1(password))), {headers: this.headers})
      .toPromise()
      .then(resp => { logg(resp);
        if(resp.json().token != null)
        {
          localStorage.setItem('token', resp.json().token)
          localStorage.setItem('username', userName)
          this.isLoggedIn = true
          this.userName = "user"
          this.subject.next("user")
          return true
        }
    else
        return false;
  
  })
      .catch(err => this.handleError(err))
  }

  checkIfIsLoggedIn(): boolean {
    //let res =  await new Promise((res, rej) => {})
    if (localStorage.getItem('token') && localStorage.getItem('username'))
    {
      this.isLoggedIn = true
      this.userName = "user"
      return true
    }

    return false
  }

  getUser(): Promise<string> {
    return new Promise<string>((res, rej) => res(this.userName));
  }

  subscribeToUser(callbackfn):void {
    this.subject.subscribe(user => callbackfn(this.userName));
  }

  isLogged(): boolean {
    return this.isLoggedIn;
  }

  logout(): void {
    this.isLoggedIn = false;
    localStorage.removeItem('token')
    localStorage.removeItem('username')
    this.userName = ""
    this.subject.next(this.userName);
  }

  registerUser(username: string, email: string, password:string): Promise<string> {
    let a = sha1(password)
    console.log("password:"+a)
    return this.http.post(apiUrl + '/register', JSON.stringify({username: username, password: a, email: email}), {headers: this.headers})
      .toPromise().then(resp => logg(resp).json() as string)
      .catch(this.handleError)
  }
}
