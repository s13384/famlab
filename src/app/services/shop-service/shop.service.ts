import { Injectable } from '@angular/core';
import { Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Product } from '../../classes/product';
import { Shop } from '../../classes/shop';
import { apiUrl } from '../../../environments/apiUrl';

@Injectable()
export class ShopService {

    private shopsUrl= apiUrl + '/shops';

    constructor(private http: Http){}

    getShops(): Promise<Shop[]> {
        return this.http.get(this.shopsUrl + '/')
            .toPromise()
            .then(response => response.json().shops as Shop[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    getShop(id: number): Promise<Shop> {
        const url = `${this.shopsUrl}/${id}`;
        return this.http.get(url)
          .toPromise()
          .then(response => response.json() as Shop)
          .catch(this.handleError);
    }

    private headers = new Headers({'Content-Type' : 'application/json'});
    update(shop: Shop): Promise<Shop> {
        const url = `${this.shopsUrl}/${shop.id}`;
        return this.http
        .put(url, JSON.stringify(shop), {headers: this.headers})
        .toPromise()
        .then(()=> shop)
        .catch(this.handleError);
    }

    create(name: string, description: string, products: number[]): Promise<Shop> {
        return this.http
            .post(this.shopsUrl, JSON.stringify({name: name, description: description, products: products}), {headers: this.headers})
            .toPromise()
            .then(res => res.json().data as Shop)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        const url = `${this.shopsUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
        .toPromise()
        .then(()=>null)
        .catch(this.handleError);
    }
}