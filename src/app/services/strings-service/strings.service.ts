import { Injectable } from '@angular/core';

@Injectable()
export class StringsService {

  //dashboard
  public Dashboard: string
  public AppName: string
  public TopProducts: string
  public ProductSearch: string
  //login component
  public WrongLoginInfo: string
  public UsernamePlaceholder: string
  public PasswordPlaceholder: string
  public LoginButtonLabel: string
  //products component
  public Products: string
  //product details component
  public Id: string
  public Details: string
  public Description: string
  public Price: string
  public AvaliableIn: string
  //cart info component
  public TotalPrice: string
  public ItemsInCart: string
  //
  public AddButtonLabel: string
  public UnnamedList: string
  public EmptyList: string
  public NewShoppingList: string
  public PageNotFound: string
  public From: string
  public ActiveList: string
  public Shops: string
  public ShoppingLists: string
  public DeleteButton: string

  public Shop: string
  public Location: string
  public Rating: string
  public PhoneNumber: string
  public Address: string
  public Email: string
  public Register: string
  public ChangListName: string
  public ListNamePlaceholder: string
  public ClearWholeList: string
  public ListRemoval: string

  constructor() { this.setPolish()}

  private setPolish(): void {
    this.Dashboard = 'Strona Główna'
    this.AppName = 'Planer Zakupów'
    this.TopProducts = 'Najlepsze Produkty'
    this.ProductSearch = 'Wyszukaj Produkt'

    this.WrongLoginInfo = 'błędny login lub hasło'
    this.UsernamePlaceholder = 'login'
    this.PasswordPlaceholder = 'hasło'
    this.LoginButtonLabel = 'Login'

    this.Products = 'Produkty'

    this.Id = 'Id:'
    this.Details = 'Szegóły:'
    this.Description = 'Opis:'
    this.Price = 'Cena:'
    this.AvaliableIn = 'Dostępny w:'

    this.TotalPrice = 'Razem:'
    this.ItemsInCart = 'Przedmioty na liście:'

    this.AddButtonLabel = 'Dodaj'
    this.UnnamedList = 'Lista bez nazwy'
    this.EmptyList = 'Pusta lista'
    this.NewShoppingList = 'Nowa lista'
    this.PageNotFound = '404 Nie znaleziono strony'
    this.From = 'Z'
    this.ActiveList = 'Aktywna Lista'
    this.Shops = 'Sklepy'
    this.ShoppingLists = 'Moje listy'
    this.DeleteButton = 'Usuń'

    this.Shop = 'Sklep:'
    this.Location = 'Lokalizacja:'
    this.Rating = 'Ocena:'
    this.PhoneNumber = 'Numer tel.'
    this.Address = 'Adres:'
    this.Email = 'Email:'
    this.Register = 'Rejestracja'
    this.ChangListName = 'Zmień nazwę listy'
    this.ListNamePlaceholder = 'Nazwa Listy'
    this.ClearWholeList = 'Wyczyść całą listę'
    this.ListRemoval = 'Czy napewno chcesz usunąć całą listę ?'
  }

  private setEnglish(): void {
    this.Dashboard = 'Dashboard'
    this.AppName = 'Shopping Planner'
    this.TopProducts = 'Top Products'
    this.ProductSearch = 'Product Search'

    this.WrongLoginInfo = 'wrong username or password'
    this.UsernamePlaceholder = 'username'
    this.PasswordPlaceholder = 'password'
    this.LoginButtonLabel = 'Login'

    this.Products = 'Products'

    this.Id = 'Id:'
    this.Details = 'Details:'
    this.Description = 'Description:'
    this.Price = 'Price:'
    this.AvaliableIn = 'Avaliable in:'

    this.TotalPrice = 'Total price:'
    this.ItemsInCart = 'Items in list:'

    this.AddButtonLabel = 'Add'
    this.UnnamedList = 'Unnamed List'
    this.EmptyList = 'Empty List'
    this.NewShoppingList = 'New shopping list'
    this.PageNotFound = '404 Page not found'
    this.From = 'From'
    this.ActiveList = 'Active List'
    this.Shops = 'Shops'
    this.ShoppingLists = 'My lists'
    this.DeleteButton = 'Remove'

    this.Shop = 'Shop:'
    this.Location = 'Location:'
    this.Rating = 'Rating:'
    this.PhoneNumber = 'Phone number:'
    this.Address = 'Address:'
    this.Email = 'Email:'
    this.Register = 'Register'
    this.ChangListName = 'Change List Name'
    this.ListNamePlaceholder = 'List Name'
    this.ClearWholeList = 'Clear whole list'
    this.ListRemoval = 'Are you sure you want to clear whole list ?'
  }
}
