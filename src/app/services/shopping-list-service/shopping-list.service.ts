import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';

import { Product } from '../../classes/product';
import { ShoppingListItem } from '../../classes/shopping-list-item';
import { ShoppingList } from '../../classes/shopping-list';
import { LoginService } from '../login-service/login.service';
import { User } from '../../classes/user';
import { StringsService } from '../../services/strings-service/strings.service';
import { Http, Headers, RequestOptions } from '@angular/http';
import { apiUrl } from '../../../environments/apiUrl'
import { logg } from '../../../environments/logg';

@Injectable()
export class ShoppingListService {
    
    private shoppingLists: ShoppingList[];
    private currentShoppingList: ShoppingList;

    changed: Subject<boolean>;

    constructor(private loginService: LoginService,
        private strings: StringsService,
        private http: Http
    ){
        this.changed = new Subject<boolean>()
        this.clearShoppingLists()
        this.loginService.subscribeToUser(u => this.onUserChange(u))
        this.loginService.getUser().then(u => this.onUserChange(u))
        this.refreshShoppingLists()
    }

    clearShoppingLists(): void {
        this.currentShoppingList = new ShoppingList;
        this.currentShoppingList.id = 0;
        this.currentShoppingList.name = this.strings.UnnamedList;
        this.shoppingLists = [];
        this.shoppingLists.push(this.currentShoppingList);
        this.changed.next(true)
    }

    time: number = 0;
    async refreshShoppingLists() {
        let timeChange = 5*1000
        for (let i=0; true ; i++){
            console.log("time="+this.time+"+"+timeChange)
            this.time+=timeChange
            if(this.isLogged())
                this.onUserChange('user')
            let a = await new Promise((resolve)=> setTimeout( () =>{return resolve()}, timeChange))
            //let a = await setTimeout(() => {return 3}, 5000)
        }
        //setTimeout(function (func) {func()}, timeChange, [func])
    }

    onUserChange(userName: string): void {

        /*if (localStorage.getItem('token') !== undefined)
        {
            this.saveState()
            this.clearShoppingLists()
        }*/
        if (userName === 'user') {
            this.__getShoppingLists().then(shls => this.shoppingLists = shls)
            .then(() => this.resetIfEmpty())
            .then(() => { 
                console.log("USER HAS SHP LIST");
                let curshpId
                if (localStorage.getItem('currentShoppingListId') != null && localStorage.getItem('currentShoppingListId') != undefined)
                    curshpId = +localStorage.getItem('currentShoppingListId')
                else
                    curshpId = 0;
                console.log("curshpId="+curshpId)
                let index = curshpId != 0 ? this.shoppingLists.findIndex((shpli) => shpli.id == curshpId) : -1
                console.log("index:"+index)
                this.currentShoppingList = this.shoppingLists[ index != -1 ? index : 0]})
            .then(() => this.changed.next(true));
        }
        else {
            console.log("USER HAS NO LIST");
            this.clearShoppingLists()
            if (localStorage.getItem('shoppinglist') != null)
            {
                this.currentShoppingList = JSON.parse(localStorage.getItem('shoppinglist'))
                this.shoppingLists = [];
                this.shoppingLists.push(this.currentShoppingList);
            }
        }
    }

    resetIfEmpty(): void {
        if (this.shoppingLists.length == 0)
            this.clearShoppingLists()
    }

    saveState(): void {
    }

    addProduct(product: Product): Promise<ShoppingList> {
        if(this.isLogged()) {
            return this.__addProduct(product.id, this.currentShoppingList.id)
            .then(shli => this.currentShoppingList = logg(shli))
            .then(() => {
                let index = this.shoppingLists.findIndex(shpli => {return shpli.id == this.currentShoppingList.id})
                this.shoppingLists[index]= this.currentShoppingList;
            })
            .then(() => {console.log("TE LISTY ADD ="); console.log(this.shoppingLists); this.currentShoppingList = this.shoppingLists.find(shpli => shpli.id == this.currentShoppingList.id)})
            .then(() => this.changed.next(true))
            .then(() => { return this.currentShoppingList});
        } else {
            let id = this.currentShoppingList.products.findIndex(shopLi => shopLi.product.id === product.id);
            if (id === -1){
                let newItem: ShoppingListItem;
                newItem= new ShoppingListItem();
                newItem.count = 1;
                newItem.product = product;
                this.currentShoppingList.products.push(newItem); 
            }
            else {
                this.currentShoppingList.products.find(item => item.product.id === product.id).count++;
            }
            this.currentShoppingList.totalPrice += product.price;
            localStorage.setItem('shoppinglist', JSON.stringify(this.currentShoppingList))
            return new Promise((res) => res(this.currentShoppingList))
        }
    }

    removeProduct(product: Product): Promise<ShoppingList> {
        if(this.isLogged()) {
            return this.__decreaseProduct(product.id, this.currentShoppingList.id)
            .then(shli => this.currentShoppingList = shli)
            .then(() => {
                let index = this.shoppingLists.findIndex(shpli => {return shpli.id == this.currentShoppingList.id})
                this.shoppingLists[index]= this.currentShoppingList;
            })
            .then(() => {console.log("TE LISTY DECREASE ="); console.log(this.shoppingLists); this.currentShoppingList = this.shoppingLists.find(shpli => shpli.id == this.currentShoppingList.id)})
            .then(() => this.changed.next(true))
            .then(() => { return this.currentShoppingList});
        } else {
            let id = this.currentShoppingList.products.findIndex(p => p.product.id === product.id);
            let prod = this.currentShoppingList.products.find(p => p.product.id === product.id);
            if (id === -1)
                return;
            if (prod.count > 1)
                prod.count--;
            else if (prod.count === 1){
                this.currentShoppingList.products.splice(id, 1);
            }
            this.currentShoppingList.totalPrice = this.currentShoppingList.totalPrice - product.price;
            if (this.currentShoppingList.totalPrice < 0) this.currentShoppingList.totalPrice = 0;
            localStorage.setItem('shoppinglist', JSON.stringify(this.currentShoppingList))
            return new Promise((res) => res(this.currentShoppingList))
        }
    }

    getTotalPrice(): number {
        return this.currentShoppingList.totalPrice;
    }

    getProducts(): Observable<ShoppingListItem[]> {
        return Observable.of(this.currentShoppingList.products);
    }

    getNumberOfProducts(): number {
        return this.currentShoppingList.products.length;
    }

    createShoppingList(): void {
        if(this.isLogged())
        {
            this.__createShoppingList().then(shlis => {
                console.log("suapsdas"); 
                this.shoppingLists = logg(shlis); 
                this.currentShoppingList = this.shoppingLists[this.shoppingLists.length-1]
                localStorage.setItem('currentShoppingListId', ''+this.currentShoppingList.id)
            }).then(() => this.changed.next(true))
        }
        else 
        {

        }
    }

    private isLogged(): boolean {
        return localStorage.getItem('username') != null;
    }

    changeShoppingList(id: number): ShoppingList {
        if (this.isLogged())
        {
            let index = this.shoppingLists.findIndex(list => list.id == id);
            if (index != -1)
            {
                this.currentShoppingList = this.shoppingLists[index];
                localStorage.setItem('currentShoppingListId', ''+id)
                this.changed.next(true)
            }
            return this.currentShoppingList;
        }
        else {
            return this.currentShoppingList
        }
    }

    rtshpli(): ShoppingList {
        return this.currentShoppingList;
    }

    getCurrentShoppingList(): Promise<ShoppingList> {
        return new Promise((resolve, reject) => { resolve(this.currentShoppingList) });
    }

    /*deleteShoppingList(id: number): void {
        let index = this.shoppingLists.findIndex(list => list.id == id);
        this.shoppingLists = this.shoppingLists.slice(index, 1);
        this.changed.next(true)
    }*/

    getShoppingLists(): Promise<ShoppingList[]> {
        return new Promise((res) => {res(this.shoppingLists)});
    }

    gettShoppingLists(): Observable<ShoppingList[]> {
        return Observable.of(this.shoppingLists)
    }

    deleteShoppingList(shpLiId: number): void{
        if (this.isLogged())
        {
            console.log("removing: " + shpLiId)
            this.__removeShoppingList(shpLiId).then(shpli => this.shoppingLists = shpli).then(() => {
                let index = this.shoppingLists.findIndex((shpli) => shpli.id == this.currentShoppingList.id)
                if(index == -1)
                {
                    this.currentShoppingList = this.shoppingLists[0]
                }
            }).then(() => this.changed.next(true))
        }
        else {
            /*let index = this.shoppingLists.findIndex(list => list.id == shpLiId);
            this.shoppingLists = this.shoppingLists.slice(index, 1);
            localStorage.setItem('shoppinglist', JSON.stringify(this.currentShoppingList))  */
            console.log("unlogged" + localStorage.getItem('username'))  
            this.changed.next(true)
        }
    }

    gettCurrentShoppingList(): Observable<ShoppingList> {
        return Observable.of(this.currentShoppingList)
    }

    renameCurrentShoppingList(newName: string): Promise<ShoppingList[]> {
        if(this.isLogged()){
            return this.__renameCurrentShoppingList(newName).then(shli => {
                this.currentShoppingList = shli
                let a =this.shoppingLists.findIndex(sl => sl.id === this.currentShoppingList.id)
                this.shoppingLists[a] = this.currentShoppingList
                this.changed.next(true)
                return this.shoppingLists
            })
        } else {
            this.currentShoppingList.name = newName
            localStorage.setItem('shoppinglist', JSON.stringify(this.currentShoppingList))
            return new Promise((res) => res(this.shoppingLists))
        }
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
      }

    toggleBought(shpLiIt: ShoppingListItem): Promise<ShoppingList> {
        if(this.isLogged()){
            let url = apiUrl + `/shoppinglist/bought?username=${localStorage.getItem('username')}&itemId=${shpLiIt.id}`
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': `Bearer ${localStorage.getItem('token')}` })
            let options = new RequestOptions({headers: headers});
            return this.http.put(url, null, options).toPromise()
                .then(resp => { })
                .then(() => {
                    return this.__getShoppingLists()
                    .then(shls => this.shoppingLists = shls)
                    .then(() => this.resetIfEmpty())
                    .then(() => this.currentShoppingList = this.shoppingLists.find((shplo) => shplo.id == this.currentShoppingList.id))
                    .then(() => {return this.currentShoppingList})
                })
                .catch(this.handleError)
        } else {
            let item  = this.currentShoppingList.products.find(shpli => shpLiIt.product.id == shpli.product.id)
            item.bought = !item.bought
            return new Promise((res) => res(this.currentShoppingList))
        }
        
    }

    private __getShoppingLists(): Promise<ShoppingList[]> {
        let headers = new Headers({'Content-Type': 'application/json', 'Authorization': `Bearer ${localStorage.getItem('token')}` })
        let options = new RequestOptions({headers: headers});
        return this.http.get(apiUrl + `/shoppinglists/${localStorage.getItem('username')}`, options).toPromise()
        .then(resp => this.mapShpLists(logg(resp).json().shoppingLists) as ShoppingList[])
        .catch(this.handleError)
    }

    private __addProduct(prodId: number, shliId: number): Promise<ShoppingList> {
        let headers = new Headers({'Content-Type' : 'application/json', 'Authorization' : `Bearer ${localStorage.getItem('token')}`});
        return this.http.put(apiUrl + `/shoppinglist/addproduct?username=${localStorage.getItem("username")}&shoppingListId=${shliId}&productId=${prodId}`, 
            null, 
            {headers: headers}).toPromise()
        .then(resp => this.mapShpList(logg(resp).json()) as ShoppingList)
        .catch(this.handleError)
    }

    private __decreaseProduct(prodId: number, shliId: number): Promise<ShoppingList> {
        let headers = new Headers({'Content-Type' : 'application/json', 'Authorization' : `Bearer ${localStorage.getItem('token')}`});
        return this.http.delete(apiUrl + `/shoppinglist/decrease?username=${localStorage.getItem("username")}&shoppingListId=${shliId}&productId=${prodId}`,  
            {headers: headers}).toPromise()
        .then(resp => this.mapShpList(logg(resp).json().shoppingList) as ShoppingList)
        .catch(this.handleError)
    }

    private __createShoppingList(): Promise<ShoppingList[]> {
        let headers = new Headers({'Content-Type' : 'application/json', 'Authorization' : `Bearer ${localStorage.getItem('token')}`});
        let options = new RequestOptions({headers: headers});
        return this.http.post(apiUrl + `/shoppingList/create?username=${localStorage.getItem('username')}&name=${this.strings.UnnamedList}`, 
            null, 
            options).toPromise()
        .then(resp => {console.log("Create shp list"); return this.mapShpLists(logg(resp.json())) as ShoppingList[]})
        .catch(this.handleError)
    }

    private __renameCurrentShoppingList(newName: string): Promise<ShoppingList> {
        let headers = new Headers({'Content-Type' : 'application/json', 'Authorization' : `Bearer ${localStorage.getItem('token')}`});
        let options = new RequestOptions({headers: headers});
        return this.http.put(apiUrl + `/shoppinglist/rename?username=${localStorage.getItem('username')}&newName=${newName}&shoppingListId=${this.currentShoppingList.id}`, 
            null, 
            options).toPromise()
        .then(resp => this.mapShpList(resp.json()) as ShoppingList)
        .catch(this.handleError)
    }

    private __removeShoppingList(shpLiId: number): Promise<ShoppingList[]> {
        let headers = new Headers({'Content-Type': 'application/json', 'Authorization': `Bearer ${localStorage.getItem('token')}` })
        let options = new RequestOptions({headers: headers});
        return this.http.delete(apiUrl + `/shoppinglist/remove?username=${localStorage.getItem('username')}&shoppingListId=${shpLiId}`, options).toPromise()
        .then(resp => this.mapShpLists(logg(resp).json()) as ShoppingList[])
        .catch(this.handleError)
    }

    private mapShpLists(shpli: any): any {
        return logg(shpli).map(({id, name, products, wholePrice}) => ({id: id, name: name, products: products, totalPrice: wholePrice}) as ShoppingList)
    }

    private mapShpList(shpli: any): any {
        let arr = [];
        arr.push(shpli)
        let result = logg(arr).map(({id, name, products, wholePrice}) => ({id: id, name: name, products: products, totalPrice: wholePrice}) as ShoppingList)
        return result[0]
    }
}