import { TestBed, inject } from '@angular/core/testing';

import { Product } from '../../classes/product';
import { ShoppingListService } from './shopping-list.service';
import { ShoppingListItem } from '../../classes/shopping-list-item';

describe('ShoppingListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShoppingListService]
    });
  });

  it('should be created', inject([ShoppingListService], (service: ShoppingListService) => {
    expect(service).toBeTruthy();
  }));

  it('should add a list item', inject([ShoppingListService] , (service: ShoppingListService) => {
    let prod = new Product();
    prod.description='a';
    prod.name = 'A';
    prod.id = 1;
    prod.price = 1;
    service.addProduct(prod);
    let products;
    products = service.getProducts().map(prods => prods as ShoppingListItem[]);
    console.log(products[0]);
    expect(products[0].count).toBe(1);
  }));
});
