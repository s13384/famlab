import { TestBed, inject } from '@angular/core/testing';

import { InMemory.DataService } from './in-memory.data.service';

describe('InMemory.DataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InMemory.DataService]
    });
  });

  it('should be created', inject([InMemory.DataService], (service: InMemory.DataService) => {
    expect(service).toBeTruthy();
  }));
});
