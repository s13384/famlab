import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const products = [
      { id: 1,  name: 'Mleko', description: 'A', price: 2.39 },
      { id: 2,  name: 'Mleko', description: 'A', price: 2.12 },
      { id: 3,  name: 'Mleko', description: 'A', price: 2.51 },
      { id: 4, name: 'Chleb', description: 'B', price: 1.99 },
      { id: 5, name: 'Chleb', description: 'B', price: 2.20 },
      { id: 6, name: 'Chleb', description: 'B', price: 2.59 },
      { id: 7, name: 'Chleb bezglutenowy', description: 'J', price: 3.57 },
      { id: 8, name: 'Chleb bezglutenowy', description: 'J', price: 4.99 },
      { id: 9, name: 'Masło', description: 'C', price: 6.84 },
      { id: 10, name: 'Bułka', description: 'D', price: 0.39 },
      { id: 11, name: 'Woda', description: 'E', price: 1.31 },
      { id: 12, name: 'Kola agular dewelopera', description: 'F', price: 3.11 },
      { id: 13, name: 'Pesip', description: 'G', price: 3.09 },
      { id: 14, name: 'Woda gazowana', description: 'H', price: 1.53 },
      { id: 15, name: 'Czekolada', description: 'I', price: 3.59 },
      { id: 16, name: 'Ciasteczka bezglutenowe', description: 'K', price: 8.99 }
    ];

    const shops = [
      { id: 1, name: 'Ropuszka', description: 'AA', 
      contact: {phonenumber: '111-111-111', address: 'Galaktyczna 9i3/4', email: 'Roszpunka1-Gdansk@Roszpunka.pl' },
      location: '111.111-111.111-111.111', rating: 0.0, 
      products: [products[0], products[3], products[8], products[9], products[15]] },

      { id: 2, name: 'Stonka', description: 'BB', 
      contact: {phonenumber: '222-222-222', address: 'Atlantyda 22b/3', email: 'Stonka444@StonkaPoland.pl' }, 
      location: '222.222-222.222-222.222', rating: 0.0, 
      products: [products[1], products[4], products[6], products[11], products[14]] },

      { id: 3, name: 'Pod Mostem', description: 'CC', 
      contact: {phonenumber: '333-333-333', address: 'Spokojna 2', email: 'PodMostemGdansk@gmail.com' }, 
      location: '333.333-333.333-333.333', rating: 0.0, 
      products: [products[2], products[5], products[7], products[10], products[12], products[13]] }
    ];

    const users = [
      { id: 1, name: 'user1', password: 'password1', email: 'user1@famlab.com', shoppingLists: [] },
      { id: 2, name: 'user2', password: 'password2', email: 'user2@famlab.com', shoppingLists: [] },
      { id: 3, name: 'user3', password: 'password3', email: 'user3@famlab.com', shoppingLists: [] }
    ];

    return {products, shops, users};
  }
}