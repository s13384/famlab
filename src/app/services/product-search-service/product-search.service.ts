import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Product } from '../../classes/product';
import { apiUrl } from '../../../environments/apiUrl'
import { ProductInfo } from '../../classes/product-info';
import { logg } from '../../../environments/logg';

@Injectable()
export class ProductSearchService {

    constructor(private http: Http){}


    searchByName(term: string, page: number): Observable<{products: ProductInfo[], maxPages: number}>{
        const url = apiUrl + `/products/${term}/${page}`;
        return this.http.get(url)
            .map(response => {return { products: response.json().content as ProductInfo[], maxPages: response.json().totalPages }})
    }

    searchByPrice(price: number): Observable<ProductInfo[]>{
        return this.http.get(apiUrl +  `/products/?price=${price}`)
            .map(response => response.json().data as ProductInfo[]);
    }
}