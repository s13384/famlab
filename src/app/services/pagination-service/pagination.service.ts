import { Injectable } from '@angular/core';

@Injectable()
export class PaginationService {

  constructor() { }

  
  pageNumbers(currentPage: number, maxPages: number): number[] {
    let pages = []
    for (let i=1; i<=maxPages; i++)
    {
      if (i == 1 || i == maxPages)
      {
        pages.push(i)
      }
      else if (i < currentPage + 2 && i > currentPage - 2){
        pages.push(i)
      }
      else if (i >= currentPage + 2 && i <= currentPage + 15 || i <= currentPage - 2 && i >= currentPage - 15) {
        if(i%10 == 0 && this.abs(i-currentPage) >= 5)
        {
          if(this.abs(i-currentPage) != 15)
            pages.push(i)
        }
        /*else if (i - currentPage <= 4 && i <= 5) {
          pages.push(i)
        }*/
      }
    }
    return pages;
  }

  abs(num: number): number {
    if (num < 0)
      return -num;
    else
      return num
  }

}
