import { ProductInfo } from "./product-info";
import { Shop } from "./shop";

export class Product {
    id: number;
    price: number;
    productInfo: ProductInfo
    shop: Shop
}