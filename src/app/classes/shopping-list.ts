import { ShoppingListItem } from './shopping-list-item';

export class ShoppingList {
    constructor(){this.totalPrice=0; this.products = []; this.name = '';}
    id: number;
    name: string;
    totalPrice: number;
    products: ShoppingListItem[];
}