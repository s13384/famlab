import { ShoppingList } from './shopping-list';
export class User {
    id: number;
    name: string;
    password: string;
    email: string;
    shoppingLists: ShoppingList[];
}