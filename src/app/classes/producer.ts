import { Contact } from "./contact";

export class Producer {
    id: number;
    name: string;
    contact: Contact;
}