export class Contact {
    id: number;
    phoneNumber: string;
    address: string;
    email: string;
}