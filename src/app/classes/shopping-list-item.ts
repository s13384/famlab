import { Product } from './product';

export class ShoppingListItem {
    id: number;
    product: Product;
    count: number;
    bought: boolean;
}