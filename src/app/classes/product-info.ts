import { Producer } from "./producer";
import { Category } from "./category";

export class ProductInfo {
    id: number;
    producer: Producer;
    images: string[];
    name: string;
    description: string;
    country: string;
    thumbnail: string;
    categories: Category[];
}