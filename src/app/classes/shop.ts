/*
import { Product } from './product';
import { Contact } from './contact'
export class Shop {
    id: number;
    name: string;
    description: string;
    contact: Contact;
    location: string;
    rate: number;
    products: Product[];
}*/

import { Product } from './product';
import { Contact } from './contact'
export class Shop {
    id: number;
    contact: Contact;
    name: string;
    lattitude: number;
    longtitude: number;
    description: string;
    rate: number;
    countOfRates: number;
}
