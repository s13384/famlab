import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './services/login-service/login.service';
import { StringsService } from './services/strings-service/strings.service';
import swal from 'sweetalert'

@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: [ './app.component.css' ]

})
export class AppComponent implements OnInit, AfterViewInit{
    showLoginComponent: boolean;

    constructor(private loginService: LoginService, private router: Router,
          private strings: StringsService){
      this.showLoginComponent = (!this.loginService.isLogged() && !(this.router.url == "/login"));

    }

    ngOnInit() {
      let logged = this.loginService.checkIfIsLoggedIn()
      let e2e = false
      if (!logged && !e2e)
        //window.alert("Listy zakupowe gościa (użytkownika niezalogowanego) zapisują się tylko lokalnie - na danej przeglądarce !")
        swal({
          title: "Uwaga",
          text: "Listy zakupowe gościa (użytkownika niezalogowanego) zapisują się tylko lokalnie - na danej przeglądarce !",
          icon: "info",
          button: "OK",
        });
    }

    ngAfterViewInit() {
      
    }

    login(): void {
      let link = ['/login'];
      this.router.navigate(link);
    }

    logout(): void {
      this.loginService.logout();
      let link = ['/dashboard'];
      this.router.navigate(link);
    }
}