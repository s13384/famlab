import { AppPage } from './app.po';
import { browser, protractor } from 'protractor';

describe('Guest', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should be able to add items to shopping list', () => {
    page.navigateTo('products')
    browser.sleep(5000)
    page.beer().click()
    page.addBeer().click()
    //browser.wait(protractor.ExpectedConditions.alertIsPresent(), 500, )
    browser.switchTo().alert().accept()
    //page.navigateTo('cart')
    //page.getActiveListButton().click()
    //page.getCartPrice().then(a => (expect(a).toBe(5.99)))
    //page.getCartProductsNumber().then(a => (expect(a).toBe(1)))
    //expect(page.getCartProductsNumber().getText()).toBe('1');
    expect(page.getCartPrice().getText()).toBe('12.99');

    expect(page.getMyListsButton()).toBeDefined();
  });

  /*it('should not be able to navigate to /shoppinglists', () => {
    page.navigateTo('shoppinglists');
    //expect(page.getPageNotFound()).toBeDefined();
    
    page.navigateTo('dashboard');
    expect(page.getMyListsButton()).toBeUndefined();
  });*/
  
});
