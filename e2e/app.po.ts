import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(target: string) {
    return browser.get('/' + target);
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getLoginUsernameInput() {
    return element(by.name('name'))
  }

  getLoginPasswordInput() {
    return element(by.name('password'))
  }

  getLoginButton() {
    return element(by.name('loginbt'))
  }

  getLogoutButton() {
    return element(by.name('logoutbt'))
  }

  getMyListsButton() {
    return element(by.cssContainingText('a', 'Moje listy'))
  }

  getPageNotFound() {
    return element(by.cssContainingText('p', ''))
  }

  beer() {
    return element(by.cssContainingText('td', 'Piwo Heinz 0.5l'))
  }

  addBeer() {
    return element(by.cssContainingText('button', 'Dodaj'))
  }

  milk() {
    return element(by.cssContainingText('td', 'Mleko UHT 2.0%'))
  }

  addMilk() {
    return element(by.cssContainingText('button', 'Dodaj'))
  }

  getCartName() {
    return element(by.id('cart-shl-name'))
  }

  getCartPrice() {
    return element(by.id('cart-tot-price'))
  }

  getCartProductsNumber() {
    return element(by.id('cart-prods-num'))
  }

  getActiveListButton() {
    return element(by.cssContainingText('a', 'Aktywna Lista'))
  }

  newListButton() {
    return element(by.buttonText('Nowa lista'))  
  }

  productsPageButton() {
    return element(by.cssContainingText('a', 'Produkty'))
  }

  myListsPage() {
    return element(by.cssContainingText('a', 'Moje listy'))
  }
}
