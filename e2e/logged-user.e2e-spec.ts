import { AppPage } from './app.po';
import { browser } from 'protractor';

describe('Logged user', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should be able to login with right credentials', () => {
    page.navigateTo('login');
    page.getLoginUsernameInput().sendKeys('Plohej');
    page.getLoginPasswordInput().sendKeys('password');
    page.getLoginButton().click();
    expect(page.getMyListsButton()).toBeDefined();
    page.getLogoutButton().click();
  });

  it('should not be able to login with wrong credentials', () => {
    page.navigateTo('login');
    page.getLoginUsernameInput().sendKeys('Plohej');
    page.getLoginPasswordInput().sendKeys('badPasswordssssssssssssssssssssssssssssssssssssss');
    page.getLoginButton().click();
    expect(page.getMyListsButton().isPresent()).toBeFalsy();
  });

  it('should be able to add items to shopping list', () => {
    page.navigateTo('login');
    page.getLoginUsernameInput().sendKeys('Plohej');
    page.getLoginPasswordInput().sendKeys('password');
    page.getLoginButton().click();
    page.myListsPage().click()
    page.newListButton().click()
    page.productsPageButton().click();
    browser.sleep(5000)
    page.milk().click();
    page.addMilk().click();
    browser.switchTo().alert().accept()
    page.beer().click()
    page.addBeer().click()
    browser.switchTo().alert().accept()
    expect(page.getCartPrice().getText()).toBe('16.13')
  });
  
});
